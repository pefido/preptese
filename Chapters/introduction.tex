\chapter{Introduction}
\label{cha:introduction}

\section{Context}
\label{sec:context}
Web applications have taken a central role in access to remote services. Traditionally, clients would only act as an end interface of the data from/to the server, not keeping data nor making computation, with the exception of computation related to data presentation. With the grow of processing power and storage capabilities of commodity hardware, such as PCs, Laptops, SmartPhones and Tablets, there has been a tendency to move some of the computation and data to these devices, instead of always relying on the servers.\par
	Peer-to-peer technologies have been around for long, but it has not been used in web applications due to the difficulty of having one client communicating with other clients. When communicating directly among clients, there can be benefits in terms of \begin{enumerate*}[(i)]
	\item latency, since devices can be closer to each other than to a server;
	\item scaling, work can be partitioned between peers;
	\item availability, because a service doesn't need to stop if the central server is temporarily down.\end{enumerate*}
Recent technology developments in this area, such as WebRTC, STUN and TURN, allowed  browser-to-browser communication with no need for native applications or browser plugins.\par
	As an example of this, Legion is a newly created framework which explores these technologies and allows client web applications to replicate data from servers, and synchronize these replicas directly among them. Browser-to-browser communication is useful in web applications that exhibit frequent interchange of information or sharing of contents between clients, such as collaborative editing.\par
	Although replicating data between web clients is a promising approach, personal devices can be unstable compared to the use of a centralized component server because they join and leave a network very often, so data persistency cannot be fully delegated to these devices. There is a need store data periodically in a more stable storage system.

\section{Motivating Problem and Solution}
\label{sec:motivating_problem_and_solution}
The processing power and the amount of storage of user devices allow current systems to transfer part of the work and data from a central server (or servers) to these end-point devices. There has been an effort to build tools that facilitate programmers to develop software that takes advantage of direct communication between clients.\par
	In this scenario, communication between browsers is possible with minimal effort from both developer and user, thanks to frameworks like WebRTC that enable direct real time communication between browsers. With WebRTC it is possible to stream data, audio, or video between browsers with no need for plugins or extensions.\par
	WebRTC made possible frameworks like Legion. Legion allows client web applications to replicate data from servers, and synchronize these replicas directly among them. This can have major impact in areas like collaborative editing, where current approaches like Google Docs always use a central server do mediate communication between clients. Legion offers the same API as Google Drive Realtime, thus allowing to easily port existing applications to the new framework.\par
	However, there are some disadvantages in direct browser-to-browser communication. User devices are unstable when compared to a server on a data center, as they can join and leave the system frequently. This makes imperative to include in the system a centralized component where data durability is guaranteed. Part of the goal of this thesis is to study how to integrate well known legacy distributed storage systems with the Legion framework, giving this framework more robustness when it comes to persistence of data and allowing clients that don't support WebRTC to use the framework as an old fashion centralized server approach.\par
	This integration will have the following main challenges. First, the incompatibility  between the Legion framework data model and the storage systems data models. To address this challenge, it will be necessary to both extend the Legion data model and to create a mapping between the two models. Second, the need to keep data synchronized between the central storage and the Legion framework, considering that replicas can be modified concurrently. This encompasses the following problems: \begin{enumerate*}[(i)]
		\item identifying the updates that have been executed in the central server and in Legion; 
		\item propagate the updates across systems efficiently. 
	\end{enumerate*}
	Besides this, and based on the study of distributed storage systems techniques, an important topic is consistency guarantees in the data propagation. Most well known storage systems offer some sort of consistency policy, whether this is atomic transactions, eventual consistency, or causal consistency. Currently, Legion support causal consistency for each object, but not across objects. In this work, we will study how to bring causal consistency between objects to the client side. This will help peer-to-peer application developers to have an easier time reasoning about data propagation between nodes.

\section{Expected Contributions}
\label{sec:expected_contributions}
The planned work for this thesis will be based on the understanding of Legion and the study of well known legacy distributed storage systems. In section \ref{sec:proposed_solution} a more in depth description of the planned work is given. The expected contributions of this work will be:
	
\begin{itemize}
\item Extend Legion to support integration with existing storage systems, such as Antidote, Riak and Cassandra.

\item The extension of Legion to support causal consistency across objects, thus providing the same consistency level of some of the storage systems.

\end{itemize}

\section{Document Structure}
\label{document_structure}
The remainder of this document is organized as follows:\par

\begin{description}
\item[Chapter \ref{cha:related_work}] describes the related work. Existing work is explored in the areas of communication technologies, peer-to-peer systems and distributed storage systems.

\item[Chapter \ref{cha:proposed_work}] discusses the proposed solution and work plan for the duration of this thesis.
\end{description}